<?php

$config = new PhpCsFixer\Config;
$config
    ->getFinder()
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests')
;

$config
    ->setRiskyAllowed(true)
    ->setCacheFile(__DIR__ . '/.php_cs.cache')
    ->setRules([
        'php_unit_method_casing' => false,
    ])
;

return $config;
