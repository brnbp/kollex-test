<?php

require __DIR__ . '/vendor/autoload.php';

use kollex\Service\ImportProducts;

$filename = sprintf('%s/%s', __DIR__, $argv[1]);

echo (new ImportProducts())->import($filename);

function dd(...$args){print_r($args);die;}
