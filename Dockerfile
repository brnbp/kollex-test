FROM php:7.4

RUN apt-get update && apt-get install -y git curl wget zip

RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN chmod +x composer.phar
RUN mv composer.phar composer
RUN mv composer /usr/local/bin
