<?php
namespace Tests\Unit\kollex\Dataprovider;

use kollex\Dataprovider\CsvDataProvider;
use kollex\Dataprovider\DataProviderFactory;
use kollex\Dataprovider\JsonDataProvider;
use kollex\Exception\UnsupportedFileExtensionException;
use Tests\TestCase;

class DataProviderFactoryTest extends TestCase
{
    /** @test */
    public function it_should_create_csv_data_provider_instance()
    {
        // Set
        $filename = 'path/file.csv';

        // Action
        $dataProvider = DataProviderFactory::create($filename);

        // Assertion
        $this->assertInstanceOf(CsvDataProvider::class, $dataProvider);
    }

    /** @test */
    public function it_should_create_json_data_provider_instance()
    {
        // Set
        $filename = 'path/file.json';

        // Action
        $dataProvider = DataProviderFactory::create($filename);

        // Assertion
        $this->assertInstanceOf(JsonDataProvider::class, $dataProvider);
    }

    /** @test */
    public function it_should_throw_exception_when_passing_invalid_file_extension()
    {
        // Set
        $filename = 'path/file.jpeg';

        // Expectation
        $this->expectException(UnsupportedFileExtensionException::class);
        $this->expectErrorMessage('Unable to read file. Check if the extension is supported. Valid extensions: csv and json, given: jpeg');

        // Action
        DataProviderFactory::create($filename);
    }
}
