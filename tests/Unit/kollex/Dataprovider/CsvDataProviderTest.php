<?php

namespace Tests\Unit\kollex\Dataprovider;

use kollex\Dataprovider\CsvDataProvider;
use kollex\Transformer\CsvDataTransformer;
use Tests\TestCase;

class CsvDataProviderTest extends TestCase
{
    /** @test */
    public function it_should_get_products()
    {
        // Set
        $file = $this->getFixture('wholesaler-full.csv');
        $dataProvider = new CsvDataProvider($file, new CsvDataTransformer());
        $expectedProducts = '[{"id":"12345600001","gtin":"23880602029774","manufacturer":"Drinks Corp.","name":"Soda Drink, 12 * 1,0l","packaging":"CA","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":1,"baseProductQuantity":12},{"id":"12345600002","gtin":"23880602029781","manufacturer":"Drinks Corp.","name":"Orange Drink, 20 * 0,5l","packaging":"CA","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":0.5,"baseProductQuantity":20},{"id":"12345600003","gtin":"23880602029798","manufacturer":"Drinks Corp.","name":"Beer, 6 * 0,5l","packaging":"BX","baseProductPackaging":"CN","baseProductUnit":"LT","baseProductAmount":0.5,"baseProductQuantity":6},{"id":"12345600004","gtin":"23880602029767","manufacturer":"Drinks Corp.","name":"Champagne, 1 * 0,75l","packaging":"BO","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":0.75,"baseProductQuantity":1}]';

        // Action
        $products = $dataProvider->getProducts();

        // Assertion
        $this->assertSame($expectedProducts, json_encode($products));
    }
}
