<?php

namespace Tests\Unit\kollex\Transformer;

use kollex\Transformer\JsonDataTransformer;
use PHPUnit\Framework\TestCase;

class JsonDataTransformerTest extends TestCase
{
    /** @test */
    public function it_should_transform_json_data()
    {
        // Set
        $transformer = new JsonDataTransformer();
        $item = [
            'PRODUCT_IDENTIFIER' => '12345600001',
            'EAN_CODE_GTIN' => '23880602029774',
            'BRAND' => 'Drinks Corp.',
            'NAME' => 'Soda Drink, 12 x 1,0l',
            'PACKAGE' => 'case 12',
            'VESSEL' => 'bottle',
            'LITERS_PER_BOTTLE' => '1.0l',
        ];

        $expectedTransformedItem = [
            'id' => '12345600001',
            'gtin' => '23880602029774',
            'manufacturer' => 'Drinks Corp.',
            'name' => 'Soda Drink, 12 x 1,0l',
            'packaging' => 'CA',
            'baseProductAmount' => 1.0,
            'baseProductPackaging' => 'BO',
            'baseProductQuantity' => 12,
            'baseProductUnit' => 'LT',
        ];

        // Action
        $transformedItem = $transformer->transform($item);

        // Assertions
        $this->assertSame($expectedTransformedItem, $transformedItem);
    }
}
