<?php

namespace Tests\Unit\kollex\Transformer;

use kollex\Transformer\CsvDataTransformer;
use PHPUnit\Framework\TestCase;

class CsvDataTransformerTest extends TestCase
{
    /** @test */
    public function it_should_transform_csv_data()
    {
        // Set
        $transformer = new CsvDataTransformer();
        $item = [
            'id' => '12345600001',
            'ean' => '23880602029774',
            'manufacturer' => 'Drinks Corp.',
            'product' => 'Soda Drink, 12 * 1,0l',
            'packaging product' => 'case 12',
            'packaging unit' => 'bottle',
            'amount per unit' => '1.0l',
        ];

        $expectedTransformedItem = [
            'id' => '12345600001',
            'gtin' => '23880602029774',
            'manufacturer' => 'Drinks Corp.',
            'name' => 'Soda Drink, 12 * 1,0l',
            'packaging' => 'CA',
            'baseProductAmount' => 1.0,
            'baseProductPackaging' => 'BO',
            'baseProductQuantity' => 12,
            'baseProductUnit' => 'LT',
        ];

        // Action
        $transformedItem = $transformer->transform($item);

        // Assertions
        $this->assertSame($expectedTransformedItem, $transformedItem);
    }
}
