<?php

namespace Tests\Unit\kollex\Service;

use kollex\Service\CsvReader;
use Tests\TestCase;

class CsvReaderTest extends TestCase
{
    /** @test */
    public function it_should_read_csv_with_comma_delimiter()
    {
        // Set
        $reader = new CsvReader(',');
        $file = $this->getFixture('wholesaler-comma-separator.csv');
        $expectedData = [
            'id' => '12345600001',
            'ean' => '23880602029774',
            'manufacturer' => 'Drinks Corp.',
            'product' => 'Soda Drink',
        ];

        // Action
        $data = $reader->read($file);

        // Action
        $data->next();$data->next();
        $this->assertSame($expectedData, $data->current());
    }

    /** @test */
    public function it_should_read_csv_with_semicolon_delimiter()
    {
        // Set
        $reader = new CsvReader();
        $file = $this->getFixture('wholesaler.csv');
        $expectedData = [
            'id' => '12345600001',
            'ean' => '23880602029774',
            'manufacturer' => 'Drinks Corp.',
            'product' => 'Soda Drink',
        ];

        // Action
        $data = $reader->read($file);

        // Action
        $data->next();$data->next();
        $this->assertSame($expectedData, $data->current());
    }
}
