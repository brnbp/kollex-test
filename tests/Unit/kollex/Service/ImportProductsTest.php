<?php
namespace Tests\Unit\kollex\Service;

use kollex\Exception\CannotReadFileException;
use kollex\Service\ImportProducts;
use Tests\TestCase;

class ImportProductsTest extends TestCase
{
    /** @test */
    public function it_should_import_products()
    {
        // Set
        $file = $this->getFixture('wholesaler-full.csv');
        $importer = new ImportProducts();
        $expectedData = '[{"id":"12345600001","gtin":"23880602029774","manufacturer":"Drinks Corp.","name":"Soda Drink, 12 * 1,0l","packaging":"CA","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":1,"baseProductQuantity":12},{"id":"12345600002","gtin":"23880602029781","manufacturer":"Drinks Corp.","name":"Orange Drink, 20 * 0,5l","packaging":"CA","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":0.5,"baseProductQuantity":20},{"id":"12345600003","gtin":"23880602029798","manufacturer":"Drinks Corp.","name":"Beer, 6 * 0,5l","packaging":"BX","baseProductPackaging":"CN","baseProductUnit":"LT","baseProductAmount":0.5,"baseProductQuantity":6},{"id":"12345600004","gtin":"23880602029767","manufacturer":"Drinks Corp.","name":"Champagne, 1 * 0,75l","packaging":"BO","baseProductPackaging":"BO","baseProductUnit":"LT","baseProductAmount":0.75,"baseProductQuantity":1}]';

        // Action
        $data = $importer->import($file);

        // Assertion
        $this->assertSame($expectedData, $data);
    }

    /** @test */
    public function it_should_throw_exception_when_trying_to_import_product_with_invalid_file()
    {
        // Set
        $file = 'invalid/file.csv';
        $importer = new ImportProducts();

        // Expectations
        $this->expectException(CannotReadFileException::class);
        $this->expectErrorMessage('Unable to read input file. Check if it exists or if the path is correct. File: invalid/file.csv');

        // Action
        $importer->import($file);
    }
}
