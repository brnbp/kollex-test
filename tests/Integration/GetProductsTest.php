<?php

namespace Tests\Integration;

use Tests\TestCase;

class GetProductsTest extends TestCase
{
    /** @test */
    public function it_should_get_products_with_csv_file()
    {
        // Set
        $expectedProducts = [
            [
                'id' => '12345600001',
                'gtin' => '23880602029774',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Soda Drink, 12 * 1,0l',
                'packaging' => 'CA',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 1,
                'baseProductQuantity' => 12,
            ],
            [
                'id' => '12345600002',
                'gtin' => '23880602029781',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Orange Drink, 20 * 0,5l',
                'packaging' => 'CA',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.5,
                'baseProductQuantity' => 20,
            ],
            [
                'id' => '12345600003',
                'gtin' => '23880602029798',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Beer, 6 * 0,5l',
                'packaging' => 'BX',
                'baseProductPackaging' => 'CN',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.5,
                'baseProductQuantity' => 6,
            ],
            [
                'id' => '12345600004',
                'gtin' => '23880602029767',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Champagne, 1 * 0,75l',
                'packaging' => 'BO',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.75,
                'baseProductQuantity' => 1,
            ],
        ];

        // Action
        $path = 'data/wholesaler_a.csv';
        $output = shell_exec("php index.php {$path}");

        // Assertions
        $array = json_decode($output, true);
        $this->assertSame($expectedProducts[0], $array[0]);
        $this->assertSame($expectedProducts[1], $array[1]);
        $this->assertSame($expectedProducts[2], $array[2]);
        $this->assertSame($expectedProducts[3], $array[3]);
    }

    /** @test */
    public function it_should_get_products_with_json_file()
    {
        // Set
        $expectedProducts = [
            [
                'id' => '12345600001',
                'gtin' => '24880602029766',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Soda Drink, 12x 1L',
                'packaging' => 'CA',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 1,
                'baseProductQuantity' => 12,
            ],
            [
                'id' => '12345600002',
                'gtin' => '24880602029773',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Orange Drink, 20x 0.5L',
                'packaging' => 'CA',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.5,
                'baseProductQuantity' => 20,
            ],
            [
                'id' => '12345600003',
                'gtin' => '24880602029780',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Beer, 6x 0.5L',
                'packaging' => 'BX',
                'baseProductPackaging' => 'CN',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.5,
                'baseProductQuantity' => 6,
            ],
            [
                'id' => '12345600004',
                'gtin' => '24880602029797',
                'manufacturer' => 'Drinks Corp.',
                'name' => 'Champagne',
                'packaging' => 'BO',
                'baseProductPackaging' => 'BO',
                'baseProductUnit' => 'LT',
                'baseProductAmount' => 0.75,
                'baseProductQuantity' => 1,
            ],
        ];

        // Action
        $path = 'data/wholesaler_b.json';
        $output = shell_exec("php index.php {$path}");

        // Assertions
        $array = json_decode($output, true);
        $this->assertSame($expectedProducts[0], $array[0]);
        $this->assertSame($expectedProducts[1], $array[1]);
        $this->assertSame($expectedProducts[2], $array[2]);
        $this->assertSame($expectedProducts[3], $array[3]);
    }
}
