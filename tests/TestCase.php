<?php

namespace Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    protected function getFixture(string $fixture): string
    {
        return __DIR__ . '/fixtures/' . $fixture;
    }
}
