<?php

namespace kollex\Service;

use Iterator;
use League\Csv\Reader;

class CsvReader
{
    protected string $delimiter = ';';

    public function __construct(string $delimiter = ';')
    {
        $this->delimiter = $delimiter;
    }

    public function read(string $pathFile): Iterator
    {
        return Reader::createFromPath($pathFile)
            ->setDelimiter($this->delimiter)
            ->fetchAssoc();
    }
}
