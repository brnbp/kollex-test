<?php
namespace kollex\Service;

use kollex\Dataprovider\DataProviderFactory;
use kollex\Exception\CannotReadFileException;

class ImportProducts
{
    public function import(string $filename): string
    {
        if (!file_exists($filename)) {
            throw new CannotReadFileException($filename);
        }

        $dataProvider = DataProviderFactory::create($filename);

        return json_encode($dataProvider->getProducts());
    }
}
