<?php

namespace kollex\Dataprovider;

use kollex\Dataprovider\Assortment\Product;
use kollex\Service\CsvReader;
use kollex\Transformer\TransformerAbstract;

class CsvDataProvider implements DataProviderInterface
{
    protected TransformerAbstract $transformer;

    protected string $filename;

    public function __construct(string $filename, TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;
        $this->filename = $filename;
    }

    public function getProducts() : array
    {
        $data = (new CsvReader())->read($this->filename);

        $products = [];
        foreach ($data as $item) {
            $products[] = new Product($this->transformer->transform($item));
        }

        return $products;
    }
}
