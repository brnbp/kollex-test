<?php

namespace kollex\Dataprovider\Assortment;

use JsonSerializable;

class Product implements ProductInterface, JsonSerializable
{
    protected string $id;
    protected string $gtin;
    protected string $manufacturer;
    protected string $name;
    protected string $packaging;
    protected string $baseProductPackaging;
    protected string $baseProductUnit;
    protected float $baseProductAmount;
    protected int $baseProductQuantity;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->gtin = $data['gtin'];
        $this->manufacturer = $data['manufacturer'];
        $this->name = $data['name'];
        $this->packaging = $data['packaging'];
        $this->baseProductPackaging = $data['baseProductPackaging'];
        $this->baseProductUnit = $data['baseProductUnit'];
        $this->baseProductAmount = $data['baseProductAmount'];
        $this->baseProductQuantity = $data['baseProductQuantity'];
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'gtin' => $this->gtin,
            'manufacturer' => $this->manufacturer,
            'name' => $this->name,
            'packaging' => $this->packaging,
            'baseProductPackaging' => $this->baseProductPackaging,
            'baseProductUnit' => $this->baseProductUnit,
            'baseProductAmount' => $this->baseProductAmount,
            'baseProductQuantity' => $this->baseProductQuantity,
        ];
    }
}
