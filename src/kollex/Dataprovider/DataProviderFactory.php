<?php

namespace kollex\Dataprovider;

use kollex\Exception\UnsupportedFileExtensionException;
use kollex\Transformer\CsvDataTransformer;
use kollex\Transformer\JsonDataTransformer;

class DataProviderFactory
{
    public static function create(string $filename): DataProviderInterface
    {
        $extension = pathinfo($filename)['extension'] ?? '';
        switch ($extension) {
            case 'csv':
                return new CsvDataProvider($filename, new CsvDataTransformer());
            case 'json':
                return new JsonDataProvider($filename, new JsonDataTransformer());
            default:
                throw new UnsupportedFileExtensionException($extension);
        }
    }
}
