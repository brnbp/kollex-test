<?php

namespace kollex\Dataprovider;

use kollex\Dataprovider\Assortment\Product;
use kollex\Transformer\TransformerAbstract;

class JsonDataProvider implements DataProviderInterface
{
    protected TransformerAbstract $transformer;

    protected string $filename;

    public function __construct(string $filename, TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;
        $this->filename = $filename;
    }

    public function getProducts() : array
    {
        $data = json_decode(file_get_contents($this->filename), true);

        $products = [];
        foreach ($data['data'] as $item) {
            $products[] = new Product($this->transformer->transform($item));
        }

        return $products;
    }
}
