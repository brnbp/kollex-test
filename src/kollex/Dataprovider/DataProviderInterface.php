<?php

namespace kollex\Dataprovider;

use kollex\Transformer\TransformerAbstract;
use kollex\Dataprovider\Assortment\ProductInterface;

interface DataProviderInterface
{
    public function __construct(string $filename, TransformerAbstract $transformer);

    /**
     * @return ProductInterface[]
     */
    public function getProducts() : array;
}
