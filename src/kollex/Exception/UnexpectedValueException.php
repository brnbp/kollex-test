<?php

namespace kollex\Exception;

use Exception;
use Throwable;

class UnexpectedValueException extends Exception
{
    public function __construct(string $field, string $input, $code = 0, Throwable $previous = null)
    {
        $message = 'Unexpected "%s" value: %s';

        parent::__construct(sprintf($message, $field, $input), $code, $previous);
    }
}
