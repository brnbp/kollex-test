<?php

namespace kollex\Exception;

use Exception;
use Throwable;

class UnsupportedFileExtensionException extends Exception
{
    public function __construct(string $extension, $code = 0, Throwable $previous = null)
    {
        $message = 'Unable to read file. Check if the extension is supported. Valid extensions: csv and json, given: %s';

        parent::__construct(sprintf($message, $extension), $code, $previous);
    }
}
