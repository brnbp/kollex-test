<?php

namespace kollex\Exception;

use Exception;
use Throwable;

class CannotReadFileException extends Exception
{
    public function __construct(string $filePath, $code = 0, Throwable $previous = null)
    {
        $message = 'Unable to read input file. Check if it exists or if the path is correct. File: %s';

        parent::__construct(sprintf($message, $filePath), $code, $previous);
    }
}
