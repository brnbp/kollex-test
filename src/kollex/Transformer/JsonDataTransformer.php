<?php

namespace kollex\Transformer;

class JsonDataTransformer extends TransformerAbstract
{
    public function transform(array $item) : array
    {
        return [
            'id' => $item['PRODUCT_IDENTIFIER'],
            'gtin' => $item['EAN_CODE_GTIN'],
            'manufacturer' => $item['BRAND'],
            'name' => $item['NAME'],
            'packaging' => $this->transformPackaging($item['PACKAGE']),
            'baseProductAmount' => $this->transformProductAmount($item['LITERS_PER_BOTTLE']),
            'baseProductPackaging' => $this->transformProductPackaging($item['VESSEL']),
            'baseProductQuantity' => $this->transformProductQuantity($item['NAME']),
            'baseProductUnit' => self::PRODUCT_UNIT_LITERS,
        ];
    }

    protected function transformProductAmount(string $productAmount): float
    {
        return (float) str_replace(',', '.', $productAmount);
    }
}
