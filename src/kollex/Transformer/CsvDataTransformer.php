<?php

namespace kollex\Transformer;

class CsvDataTransformer extends TransformerAbstract
{
    public function transform(array $item) : array
    {
        return [
            'id' => $item['id'],
            'gtin' => $item['ean'],
            'manufacturer' => $item['manufacturer'],
            'name' => $item['product'],
            'packaging' => $this->transformPackaging($item['packaging product']),
            'baseProductAmount' => (float) $item['amount per unit'],
            'baseProductPackaging' => $this->transformProductPackaging($item['packaging unit']),
            'baseProductQuantity' => $this->transformProductQuantity($item['product']),
            'baseProductUnit' => $this->transformProductUnit($item['amount per unit']),
        ];
    }
}
