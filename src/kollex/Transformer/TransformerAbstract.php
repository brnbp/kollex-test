<?php

namespace kollex\Transformer;

use kollex\Exception\UnexpectedValueException;

abstract class TransformerAbstract
{
    protected const PACKAGING_CASE = 'CA';
    protected const PACKAGING_BOX = 'BX';
    protected const PACKAGING_BOTTLE = 'BO';
    protected const PACKAGING_CAN = 'CN';
    protected const PRODUCT_UNIT_LITERS = 'LT';
    protected const PRODUCT_UNIT_GRAMS = 'GR';

    abstract public function transform(array $item) : array;

    public function map(array $items): array
    {
        return array_map([$this, 'transform'], $items);
    }

    protected function transformPackaging(string $packaging): string
    {
        if (stripos($packaging, 'case') !== false) {
            return self::PACKAGING_CASE;
        }

        if (stripos($packaging, 'box') !== false) {
            return self::PACKAGING_BOX;
        }

        if (stripos($packaging, 'single') !== false || stripos($packaging, 'bottle') !== false) {
            return self::PACKAGING_BOTTLE;
        }

        throw new UnexpectedValueException('packaging', $packaging);
    }

    protected function transformProductPackaging(string $productPackaging): string
    {
        if (stripos($productPackaging, 'bottle') !== false) {
            return self::PACKAGING_BOTTLE;
        }

        if (stripos($productPackaging, 'can') !== false) {
            return self::PACKAGING_CAN;
        }

        throw new UnexpectedValueException('product packaging', $productPackaging);
    }

    protected function transformProductUnit(string $productUnit): string
    {
        if (stripos($productUnit, 'l') !== false) {
            return self::PRODUCT_UNIT_LITERS;
        }

        if (stripos($productUnit, 'g') !== false) {
            return self::PRODUCT_UNIT_GRAMS;
        }

        throw new UnexpectedValueException('product unit', $productUnit);
    }

    protected function transformProductQuantity(string $name): int
    {
        preg_match('/\d+/', $name, $matches);

        if ($matches && !empty($matches[0])) {
            return (int) $matches[0];
        }

        return 1;
    }
}
