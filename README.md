### Build:
`$ docker-compose build`

### Install Dependencies:
`$ docker-compose run app composer install`

### Run Tests (integration and unit tests, csfixer and phpstan)
`$ docker-compose run app composer run tests`

### Run import
`$ docker-compose run app php index.php data/wholesaler_a.csv`

`$ docker-compose run app php index.php data/wholesaler_b.json`

---

#### Documentation
 - I usually try to avoid putting docblocks in code (unless there's a really complicated business logic), instead I tried to create the most self-explanatory code possible.


#### Tests
 - I created two types of tests:
    - Unit tests - focused on the smaller parts of the project, with cases for each possible path that the code can go.
    - Integration tests - tested the code directly in the entry point, going through the whole flow and checking the final output.
